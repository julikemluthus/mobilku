<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Mobilku login" />
    <meta name="keywords" content="Mobilku login " />
    <title>Mobilku Login</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/materialize.min.css">
    <!-- js script-->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/materialize.min.js"></script>
</head>

<body style="background-color: #dedede;">

    <main>

        <div class="container" style="margin-top: 50px !important;">
            <div class="center-align">
                <div class="card login hoverable" style="max-width: 360px;margin: 0 auto;">
                    <div class="card-content">

                        <form method="post" id="smul" class="col s12" onsubmit="event.preventDefault(); goNext();">
                            <div class="row">
                                <div class="input-field col s12">
                                    <input  type="email" name="email" required id="email" class="validate">
                                    <label for="email">Masukan Username</label>
                                </div>

                                <div class="input-field col s12">
                                    <input type="password" name="password" required id="password" class="validate">
                                    <label for="password">Masukan password</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="right-align">
                                    <label>
                                    </label>
                                </div>
                            </div>
                            <div class="row center-align">
                                <button type="submit" name="btn_login" class="col s12 btn btn-large waves-effect grey darken-2">Login</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
</body>

</html>
<script type="text/javascript">
  function goNext() {
    var username = $("#email").val();
    var password = $("#password").val();

    $.ajax({
      url: '<?php echo base_url('index.php/login/aksi_login')?>',
      type: 'POST',
      data: {username: username,password:password},
      success:function (data) 
      {
          if (data == "sukses") 
          {
            window.location.href = "<?php echo base_url('index.php/admin')?>";
          } 
          else
          {
            $("#smul")[0].reset();
            Materialize.toast('Fail to login', 4000) 

          }
      }
    });
  }
</script>
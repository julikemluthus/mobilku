<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Home Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Dashboard"/>
    <meta name="keywords" content="Home Dashboard"/>

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/material-design-icons/iconfont/material-icons.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/materialize.min.css">
    <link href="<?php echo base_url('assets/css/dataTables.bootstrap.min.css') ?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/') ?>button.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/') ?>tablestyle.css">

    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/materialize.min.js"></script>
    <!-- kebutuhan datatables -->    
    <script src="<?php echo base_url('assets/js/jquery.dataTables.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/dataTables.bootstrap.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/') ?>dataTables.button.min.js"></script>
    <!-- untuk kebutuhan export data -->    
    <script type="text/javascript" src="<?php echo base_url('assets/js/') ?>button.flash.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/') ?>jszip.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/') ?>pdfmake.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/') ?>vfs_fonts.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/') ?>buttons.html5.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/') ?>buttons.print.min.js"></script>
    <!-- memberi highlight / penandaan saat melakukan pencarian -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/') ?>jquery.mark.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/') ?>datatables.mark.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/') ?>sweetalert2.all.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/') ?>year-select.js"></script>
</head>
<body>
<!-- navigation -->
  <nav style="height: 64px;">
    <div class="nav-wrapper">
      <a class="brand-logo" style="margin-left: 2px; left: 60px;"><b>Mobilku</b></a>

      <ul id="nav-mobile" class="right ">
        <li class="hide-on-med-and-down"><a>Hallo <?php echo $this->session->userdata('nama');?></a></li>
        <li><a href="<?php echo base_url('index.php/admin/logout')?>"><b>Logout</b></a></li>
      </ul>      
      <ul id="nav-mobile" class="right hide-on-med-and-down">
        <li><img style="width: 55px;margin-top: 6px; " src="<?php echo base_url('assets/img/')?>gambarprofil.png"></a></li>
      </ul>      
    </div>
  </nav>
<!-- end navigation -->

<!-- content table -->
<div class="container-fluid z-depth-1 box-table">
    <table id="user_list" class="table striped" width="1200px">
        <thead class="z-depth-1">
        <tr>
            <th style="width: 30px;"></th>
            <th>Nomor Kerangka</th>
            <th>Nomor Polisi</th>
            <th>Merek</th>
            <th>Tipe</th>
            <th>Tahun</th>
            <th style="width: 220px;">Aksi</th>
        </tr>

        </thead>
        <tbody>
        </tbody>
    </table>
    <a onclick="$('#modalAdd').modal('open');"  style="margin-bottom: 15px;  float: right;margin-top: 30px;" class="btn waves-effect waves-light blue action_button">Tambah Mobil</a>    
</div>
<!-- end of content table -->
</body>
</html>
<script type="text/javascript" src="<?php echo base_url('assets/js/home.js')?>"></script>

<!-- modal untuk inserting data -->
<div id="modalAdd" class="modal modal-fixed-footer">
    <form onsubmit="event.preventDefault(); submitDataMobil();" id="insert_datamobil">
        <div class="modal-content">
            <div class="container">
                    <h5 style="margin-bottom: 30px;">Tambah Data Mobil</h5>
                    <div style="width: 100%; height: 3px; background: black;margin-top: -20px; margin-bottom: 40px;"></div>
                    <div class="row">
                        <div class="input-field col s6">
                            <input id="no_kerangka" type="text" placeholder="No Kerangka" required class="validate">
                            <label for="no_kerangka">Nomor Kerangka</label>
                        </div>

                        <div class="input-field col s6">
                            <input id="no_polisi" type="text" placeholder="No Polisi" required class="validate">
                            <label for="no_polisi">Nomor Polisi</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <input id="merek" type="text" placeholder="Merek" required class="validate">
                            <label for="merek">Merek</label>
                        </div>

                        <div class="input-field col s6">
                            <input id="tipe" type="text" placeholder="Tipe" required class="validate">
                            <label for="tipe">Tipe</label>
                        </div>
                    </div> 
                    <div class="row">
                        <div class="col s6">
                        <select class="yearselect select" id="tahun" ></select>
                        </div>
                    </div>                                        
            </div>
        </div>

            <div class="modal-footer">
                <a class="modal-close waves-effect waves-green btn-flat" onclick="$('#insert_datamobil')[0].reset();">RESET & KEMBALI</a>
                <button type="submit" class=" waves-effect waves-light btn blue darken-2">SUBMIT</button>
            </div>
    </form>
    </div>


<!-- modal untuk editing -->
<div id="modalEdit" class="modal modal-fixed-footer">
    <form onsubmit="event.preventDefault(); submitEditDataMobil();" id="edit_datamobil">
        <div class="modal-content">
            <div class="container">
                    <h5 style="margin-bottom: 30px;">Edit Data Mobil</h5>
                    <div style="width: 100%; height: 3px; background: black;margin-top: -20px; margin-bottom: 40px;"></div>
                    <div class="row">
                        <div class="input-field col s6">
                            <input id="no_kerangka_edit" type="text" placeholder="No Kerangka" required class="validate">
                            <label for="no_kerangka_edit">Nomor Kerangka</label>
                        </div>

                        <div class="input-field col s6">
                            <input id="no_polisi_edit" type="text" placeholder="No Polisi" required class="validate">
                            <label for="no_polisi_edit">Nomor Polisi</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <input id="merek_edit" type="text" placeholder="Merek" required class="validate">
                            <label for="merek_edit">Merek</label>
                        </div>

                        <div class="input-field col s6">
                            <input id="tipe_edit" type="text" placeholder="Tipe" required class="validate">
                            <label for="tipe_edit">Tipe</label>
                        </div>
                    </div> 
                    <div class="row">
                        <div class="input-field col s6">
                            <input id="tahun_edit" type="text" placeholder="Tahun" required class="validate">
                            <label for="tahun_edit">Tahun</label>
                        </div>
                    </div>
                    <input type="hidden" id="idhidden">                                        
            </div>
        </div>
            <div class="modal-footer">
                <a class="modal-close waves-effect waves-green btn-flat">RESET & KEMBALI</a>
                <button type="submit" class=" waves-effect waves-light btn blue darken-2">SUBMIT</button>
            </div>
    </form>
    </div>

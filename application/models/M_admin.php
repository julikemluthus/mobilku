<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class M_admin extends CI_Model {
    //variable data mobil
    var $column_order_produk = array('id','nomor_kerangka','nomor_polisi','merek','tipe','tahun'); //array data yang di urut
    var $column_search_produk = array('id', 'nomor_kerangka', 'nomor_polisi', 'merek', 'tipe','tahun'); //array data pencarian
    var $order_admin_produk = array('id' => 'desc'); // detault data yang di urutkan

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    # Model Logic Tabel
    private function _get_datatables_query_produk() 
    {
        $this->db->select('*');
        $this->db->from('data_mobil');
        if($this->input->post('tablecari') != "" or $this->input->post('tablecari') != null)
        {
            $tabelcari = $this->input->post('tablecari');
            $this->db->like($tabelcari, $_POST['search']['value']);
        }    

        $i = 0;

        // logic untuk mendapatkan data-data hasil pencarian
        foreach ($this->column_search_produk as $item) // men-loop kolom
        {
            if ($_POST['search']['value']) // kondisi jika menerima data "cari" dari tabel dashboard
            {
                if ($i === 0) // loop pertama
                {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search_produk) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }

            $i++;
        }

        if (isset($_POST['order'])) // logic untuk mengurutkan per kolom/field
        {
            $this->db->order_by($this->column_order_produk[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order_admin_produk)) {
            $order = $this->order_admin_produk;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    } 
 
    // fungsi yang di gunakan untuk mengantarkan data agar di olah pada controller : "listing_produk"
    function get_datatables_produk()
    {
        $this->_get_datatables_query_produk();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    
    // fungsi yang di gunakan untuk menghitung data yang di dapatkan, dari hasil filter pada controller admin : "listing_produk"
    function count_filtered_produk()
    {
        $this->_get_datatables_query_produk();
        $query = $this->db->get();
        return $query->num_rows();
    }

    // fungsi yang di gunakan untuk mendapatkan total data dari keseluruhan pada controller admin : "listing_produk"
    function count_all_produk()
    {
        $this->db->from("data_mobil");
        return $this->db->count_all_results();
    }    


    /************************/
    /*    Logika Hapus      //
    /************************/
    // fungsi yang di gunakan untuk hapus di db, pada controller admin : "delete_data" & api : "delete_data_api"
    function delete_datamobil($datawhere)
    {
        $this->db->where($datawhere);
        $this->db->delete('data_mobil');
        $affected = $this->db->affected_rows();

                if ($affected > 0) {
                    $status = "sukses";
                } else {
                    $status = "gagal";
                }

        return $status; 

    }

    /************************/
    /*    Logika Insert      //
    /************************/
    // fungsi yang di gunakan untuk memasukan data ke db, pada controller admin : "insert_data" & api : "insert_data_api"
    function insert_datamobil($datamobil)
    {
            $this->db->insert('data_mobil', $datamobil);
            $affected = $this->db->affected_rows();

            if ($affected > 0) {
                $status =  "sukses";
            } else {
                $status =  "gagal";
            }                               

            return $status;

    }

    /************************/
    /*    Logika Update      //
    /************************/
    // fungsi yang di gunakan untuk fetching data di db, pada controller admin : "get_datamobil"
    function get_datamobil_byid($id)
    {
        $this->db->select('*');
        $this->db->from("data_mobil");
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

    // fungsi yang di gunakan untuk update data di db, pada controller admin : "edit_data" & api : "edit_data_api"
    function edit_datamobil($datamobil,$datawhere)
    {
        $this->db->where($datawhere);
        $query = $this->db->update('data_mobil', $datamobil);

            $affected = $this->db->affected_rows();

            if ($affected > 0) {
                $status =  "sukses";
            } elseif($affected == 0) {
                $status =  "gagalkosong";
            }
            else{
                $status = "gagal";
            }                               
            return $status;
    }

    /*********************************************************/
    /*                 MODEL  API                            //
    /*********************************************************/
    // fungsi yang di gunakan untuk read semua data db, pada controller api : "mobil"    
    function select_all_datamobil()
    {
        $query = $this->db->get('data_mobil');  
        $result = $query->result_array();
        $dataresult = array('status' => 'getall','data'=>$result);
        return $dataresult;
    }

    // fungsi yang di gunakan untuk read data berdasarkan brand atau tipe di db, pada controller api : "mobil"    
    function select_datamobil($merek,$tipe)
    {

        $this->db->select('*');
        $this->db->from('data_mobil');

        if ($merek != '' && $tipe != '') 
        {
            $stats = "getbyboth";
            $this->db->where('merek',$merek);            
            $this->db->where('tipe', $tipe);            
        }
        elseif ($merek != '' && $tipe == '') {
            $stats = "getbymerek";            
            $this->db->where('merek',$merek);            
        }
        else{
            $stats = "empty";
            $this->db->where('merek','##TIDAK_ADA_DATA##');            
            $this->db->where('tipe','##TIDAK_ADA_DATA##');
        }                
        $query=$this->db->get();
        $result = $query->result_array();
        $dataresult = array('status' => $stats,'data'=>$result);
        return $dataresult;        
    }
}

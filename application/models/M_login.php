<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class M_login extends CI_Model {
 
 
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    // fungsi yang di gunakan untuk mendapatkan dan mencocokkan data login
    function cek_login($table,$where)
    {      
        return $this->db->get_where($table,$where);
    }   

}
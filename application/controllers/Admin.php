<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller
{
    function __construct()  // fungsi konstruktor , mengecek apakah data sesi loginnya ada
    {
        parent::__construct();
        $this->load->model('M_admin', 'dashboard');
        $this->load->library("unit_test");
        if ($this->session->userdata('status') != "login") {
            redirect(base_url("/index.php/login"));
        }
    }

    //**************************************//
    //           VIEW (listing tampilan)    //
    //**************************************//

    function index()
    {
        $this->load->view('home_dashboard');
    }

    //**********************************************************************//
    //           TABLE ( controller yang di gunakan view "home_dashboard")  //
    //**********************************************************************//
    public function listing_produk()
    {
        $list = $this->dashboard->get_datatables_produk();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $datarow) {
            $no++;
            $row = array();
            $row[] = $no.'.';
            $row[] = $datarow->nomor_kerangka;
            $row[] = $datarow->nomor_polisi;
            $row[] = $datarow->merek;
            $row[] = $datarow->tipe;
            $row[] = $datarow->tahun;
            $row[] = '<a style="margin-right:10px;" onclick="deleteData(' . "'" . $datarow->id . "'" . ')"  class="waves-effect waves-light action_button btn red darken-2">Hapus</a><a onclick="editData(' . "'" . $datarow->id . "'" . ')"  class="action_button waves-effect waves-light btn yellow darken-3">Edit</a>';

            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->dashboard->count_all_produk(),
            "recordsFiltered" => $this->dashboard->count_filtered_produk(),
            "data"            => $data,
        );
        echo json_encode($output);
    }

    //*******************************************************//
    //        GET DATA ( fetching satu data berdasarkan id ) //
    //*******************************************************//
    public function get_datamobil()
    {
        $id     = trim($this->input->post('id'));
        $result = $this->dashboard->get_datamobil_byid($id);
        echo json_encode($result);
    }


    //*****************************************************************//
    //    MASUKAN DATA ( Memasukan Data, digunakan di home dashboard ) //
    //*****************************************************************//
    function insert_data()
    {
        $nokerangka = $this->input->post('nokerangka');
        $nopolisi = $this->input->post('nopolisi');
        $merek = $this->input->post('merek');
        $tipe = $this->input->post('tipe');
        $tahun = $this->input->post('tahun');

        $datainsert = array(
                        'nomor_kerangka' => $nokerangka,
                        'nomor_polisi'   => $nopolisi,
                        'merek'          => $merek,
                        'tipe'           => $tipe,
                        'tahun'          => $tahun,
        );

        $status = $this->dashboard->insert_datamobil($datainsert);
        echo json_encode(array('status' => $status));

    }    

    //*********************************************************************************//
    //    DELETE DATA  (Menghapus data berdasarkan id, digunakan di home_dashboard )   //
    //*********************************************************************************//
    function delete_data()
    {
        $id        = trim($this->input->post('id'));
        $datawhere = array('id' => $id ,);  
        $status    = $this->dashboard->delete_datamobil($datawhere);
        echo json_encode(array('status' => $status));
    }

    //*******************************************************************************//
    //     EDIT DATA (mengedit data berdasarkan id, digunakan di home_dashboard )    //
    //*******************************************************************************//
    function edit_data()
    {
        $nokerangka = $this->input->post('nokerangka');
        $nopolisi   = $this->input->post('nopolisi');
        $merek      = $this->input->post('merek');
        $tipe       = $this->input->post('tipe');
        $tahun      = $this->input->post('tahun');
        $id         = trim($this->input->post('id'));

        $datawhere  = array('id' => $id);
        $dataupdate = array(
                        'nomor_kerangka' => $nokerangka,
                        'nomor_polisi' => $nopolisi,
                        'merek' => $merek,
                        'tipe' => $tipe,
                        'tahun' => $tahun,
        );
        $status = $this->dashboard->edit_datamobil($dataupdate,$datawhere);
        echo json_encode(array('status' => $status));
    }

    //fungsi untuk logout, dan menghapus kan sesi login
    function logout(){
        $this->session->sess_destroy();
        redirect(base_url('index.php/login'));
    }
}



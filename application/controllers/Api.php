<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('M_admin', 'api'); //include model dari M_Admin
    }

    //**************************************//
    //           VIEW (Tampilan)            //
    //**************************************//
    function index()
    {
        echo "for program interface need only !";
    }



    //*************************************************************//
    //   Request (menghandle dan cek semua request yang datang  )  //
    //*************************************************************//

    public function mobil()
    {
        $method = $_SERVER['REQUEST_METHOD']; // mengecek metode request
        if ($method == "POST") 
        {
            $this->insert_data_api(); // jika post, maka akan hit fungsi insert data
        }
        elseif ($method == "GET") { //jika get di lakukan double cek kondisi
            if (isset($_GET['BRAND']) && isset($_GET['TYPE'])) {
                $getdata = $this->api->select_datamobil($_GET['BRAND'],$_GET['TYPE']); //jika data get tersedia, maka akan excecute fungsi model untuk ambil data mobil sesuai brand dan type yang tersedia                 
            }
            elseif (!isset($_GET['BRAND']) && !isset($_GET['TYPE']))
            {
                $getdata = $this->api->select_all_datamobil(); // jika data get kosong, maka akan excecute fungsi model mengambil semua data mobil                
            }
            elseif (isset($_GET['BRAND']) && !isset($_GET['TYPE'])) {
                $getdata = $this->api->select_datamobil($_GET['BRAND'],'');// jika data get 'merek' yang tersedia, maka akan hanya mengambil data berdasarkan merek sesuai data get                  
            }
            else{
                $getdata = array('status' => 'empty'); //jika kosong, respon status kosong
            }

            echo json_encode($getdata);
        }
        elseif ($method == "PUT") { 
            $this->edit_data_api(); // jika metode put, maka akan eksekusi fungsi edit data 
        }
        elseif ($method == "DELETE") {
            $this->delete_data_api(); // jika metode delete, maka akan eksekusi fungsi delete data
        }
    }


    function insert_data_api()
    {

        $nokerangka = $this->input->post('nokerangka');
        $nopolisi   = $this->input->post('nopolisi');
        $merek      = $this->input->post('merek');
        $tipe       = $this->input->post('tipe');
        $tahun      = $this->input->post('tahun');

        if (isset($nokerangka) && isset($nopolisi) && isset($merek) && isset($tipe) && isset($tahun)) {
            $datainsert = array(
                            'nomor_kerangka' => $nokerangka,
                            'nomor_polisi'   => $nopolisi,
                            'merek'          => $merek,
                            'tipe'           => $tipe,
                            'tahun'          => $tahun,
                          );
            $status = $this->api->insert_datamobil($datainsert);
        }
        else{
            $status = "err_format";            
        }
        echo json_encode(array('status' => $status));

    }    


    //**************************************//
    //               DELETE                 //
    //**************************************//
    function delete_data_api()
    {

            if (isset($_GET['BRAND']) && isset($_GET['TYPE'])) {
                $datawhere = array('merek' => $_GET['BRAND'] , 'tipe' => $_GET['TYPE'] );                
            }
            else{
                $datawhere = array('merek' => '##NO_DATA_AVAILABLE_FOR_PARAMETERS##' , 'tipe' => '##NO_DATA_AVAILABLE_FOR_PARAMETERS##' );
            }
            //model untuk menghapus data sesuai parameter get yang di sediakan
            $status = $this->api->delete_datamobil($datawhere);
            echo json_encode(array('status' => $status)); // mengeluarkan respon status berbentuk json
    }

    //**************************************//
    //                  EDIT DATA           //
    //**************************************//
    function edit_data_api()
    {

            $nokerangka ='';  $nopolisi='';  $merek=''; $tipe=''; $tahun='';
            // mengecek apakah ada data bawaan saat request 
            if($filecontent = file_get_contents('php://input') !== false){ // jika ada
                $data       = json_decode( file_get_contents('php://input')); // merubah data request menjadi std object
                $nokerangka = $data->nokerangka;                 
                $nopolisi   = $data->nopolisi;                 
                $merek      = $data->merek;                 
                $tipe       = $data->tipe;                 
                $tahun      = $data->tahun;      

                if (isset($nokerangka) && isset($nopolisi) && isset($merek) && isset($tipe) && isset($tahun)) {
                    $dataupdate = array(
                                    'nomor_kerangka' => $nokerangka,
                                    'nomor_polisi'   => $nopolisi,
                                    'merek'          => $merek,
                                    'tipe'           => $tipe,
                                    'tahun'          => $tahun,
                            );
                    if (isset($_GET['BRAND']) && isset($_GET['TYPE'])) {
                        $datawhere = array('merek' => $_GET['BRAND'] , 'tipe' => $_GET['TYPE'] );                
                    }
                    else{
                        $datawhere = array('merek' => '##NO_DATA_AVAILABLE_FOR_PARAMETERS##' , 'tipe' => '##NO_DATA_AVAILABLE_FOR_PARAMETERS##' );
                    }                    
                    // mengubah data mobil dengan parameter data2 mobil baru, berdasarkan data get yang tersedia
                    $status = $this->api->edit_datamobil($dataupdate,$datawhere);
                }
                else{
                    $status = 'missing_param';
                }

            }
            else{
                $status = "empty_param";                       
            }
            echo json_encode(array('status' => $status));
    }
}



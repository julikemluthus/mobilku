<?php 

defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller{

	function __construct(){
		parent::__construct();		
		$this->load->model('m_login');
        if ($this->session->userdata('status') == "login") {
            redirect(base_url("/index.php/admin"));
        }		
	} 

	function index(){
		$this->load->view('login');
	}


	function Aksi_login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$where = array(
			'username' => $username,
			'password' => $password
			);
		$cek = $this->m_login->cek_login("users",$where)->num_rows(); 
		if($cek > 0)
		{
			$data_session = array(
				'nama'   => $username,
				'status' => "login",
				);
			$this->session->set_userdata($data_session);
			echo "sukses";
		}
	
		else
		{
			echo "gagal";
		}
	}

	}
?>

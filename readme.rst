# Mobilku

aplikasi dashboard mobilku memiliki beberapa kelebihan : 
1. single page, semua perubahan tetap terjadi di halaman tersebut
2. User friendly
3. UI yang menarik, dengan mengikuti standar design google "Material"
4. Mudah untuk di deploy


## Language & Technology

**UI & UX** : menggunakan CSS Framework seperti materialize, JS Framework seperti Jquery, datatable , dan sebagainya
**Backend** : menggunakan PHP framework CodeIgniter sebagai backend code


## Informasi Release

Mobilku Web Application Versi 0.0.1

## Installasi

untuk tata cara installasi mohon cek di dalam file project, arahkan ke **"data_misc"** dengan nama file **Dokumentasi_Webapp&Rest_API.pdf**




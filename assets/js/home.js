   $(document).ready(function () {
        var d = new Date();
        var yearn = d.getFullYear();
        $('.yearselect').yearselect({
            end: yearn,
        });
        $('.select').material_select();
        $(".button-collapse").sideNav();
        $('.modal').modal();
    });

    var usrlist = $("#user_list").DataTable({ //proses inisialisasi datatable
        processing: !0,
        serverSide: !0,
        order: [],
        mark: true,
        initComplete: function(settings, json) { // saat inisiasi data pertama maka akan appending list kategori
            $("#user_list_filter").append('<select onchange="ceksedia_table();" id="kriteria" class="browser-default" style="display:inline-block;width:inherit; margin-left:5px;"><option value=""  selected="">Kriteria</option><option value="nomor_kerangka">No Kerangka</option><option value="nomor_polisi">No Polisi</option><option value="merek">Merek</option><option value="tipe">Tipe</option><option value="tahun">Tahun</option></select>')
        },        
        lengthMenu: [[10, 25, 50, 100, 150, 200, 300, 400, 500, 1000, -1], [10, 25, 50, 100, 150, 200, 300, 400, 500, 1000, "All"]],
        dom: 'lBfrtip',
        buttons: [
            {
                extend: 'collection',
                className: 'btn green darken-1 waves-effect raes',
                init: function (api, node, config) {
                    $(node).removeClass('dt-button')
                },
                text: 'EKSPOR',
                buttons: [
                    {
                        extend: 'copy',
                        className: 'btn raton grey ligthen-1 waves-effect',
                        init: function (api, node, config) {
                            $(node).removeClass('dt-button')
                        },
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        }                           
                    },
                    {
                        extend: 'excelHtml5',
                        orientation: 'landscape',
                        className: 'btn grey ligthen-1 waves-effect raton',
                        init: function (api, node, config) {
                            $(node).removeClass('dt-button')
                        },
                        pageSize: 'LEGAL',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        }                           
                    },
                    {
                        extend: 'csvHtml5',
                        orientation: 'landscape',
                        className: 'btn grey ligthen-1 waves-effect raton',
                        init: function (api, node, config) {
                            $(node).removeClass('dt-button')
                        },
                        pageSize: 'LEGAL',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        }                        

                    },
                    {
                        extend: 'pdfHtml5',
                        orientation: 'landscape',
                        className: 'btn grey ligthen-1 waves-effect raton',
                        init: function (api, node, config) {
                            $(node).removeClass('dt-button')
                        },
                        pageSize: 'LEGAL',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        }                        
                    },
                    {
                        extend: 'print',
                        orientation: 'landscape',
                        className: 'btn grey ligthen-1 waves-effect raton',
                        init: function (api, node, config) {
                            $(node).removeClass('dt-button')
                        },
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        }

                    },
                ],
            },

            {
                text: 'MUAT ULANG',
                className: 'btn grey darken-1 waves-effect raes',
                init: function (api, node, config) {
                    $(node).removeClass('dt-button')
                },
                action: function () {
                    reload_table();
                }
            },
        ],
        columnDefs: [
            {
                targets: [-1],
                orderable: !1
            }],
        ajax: {
            url: "admin/listing_produk",
            type: "POST",
            data: function ( data ) {
                var searchval = $('.dataTables_filter input').val();
                if (searchval != "" && searchval != null) 
                {
                        if ($("#kriteria").val() != "" || $("#kriteria").val() != null) 
                        {
                            data.tablecari = $('#kriteria').val();
                            
                        }    
                }        
            }              
        },
        language: {
          info: "<p style='font-size:19px;'>Total Mobil : <b>_TOTAL_</b></p>",
    }        
    });



    function reload_table() {
        usrlist.ajax.reload(null, !1)
    }



    function submitDataMobil() {
        var nokerangka = $("#no_kerangka").val();
        var nopolisi = $("#no_polisi").val();
        var merek = $("#merek").val();
        var tipe = $("#tipe").val();
        var tahun = $("#tahun").val();
        $.ajax({
                url: "admin/insert_data",
                data: {
                    nokerangka: nokerangka,
                    nopolisi: nopolisi,
                    merek:merek,
                    tipe:tipe,
                    tahun:tahun,
                },
                dataType:'json',
                type: "POST",
                beforeSend: function () {
                    Materialize.toast('memasukan data..', 1000)  
                },                
                success: function(e) {
                    if (e.status == "sukses") 
                    {
                        $("#modalAdd").modal('close');
                        $("#insert_datamobil")[0].reset();
                        Materialize.toast('Berhasil memasukan data!', 2000, 'green') 
                        reload_table();
                    }
                    else{
                        Materialize.toast('Terjadi Kesalahan pada server!', 2000, 'red') 
                        reload_table();                        
                    }
                },
            })   
    }


    function submitEditDataMobil() {
        var nokerangka = $("#no_kerangka_edit").val();
        var nopolisi = $("#no_polisi_edit").val();
        var merek = $("#merek_edit").val();
        var tipe = $("#tipe_edit").val();
        var tahun = $("#tahun_edit").val();
        var id = $("#idhidden").val();
        $.ajax({
                url: "admin/edit_data",
                data: {
                    nokerangka: nokerangka,
                    nopolisi  : nopolisi,
                    merek     : merek,
                    tipe      : tipe,
                    tahun     : tahun,
                    id        : id
                },
                dataType:'json',
                type: "POST",
                beforeSend: function () {
                    Materialize.toast('mengupdate data..', 1000)  
                },                
                success: function(e) {
                    if (e.status == "sukses") 
                    {
                        $("#modalEdit").modal('close');
                        Materialize.toast('Berhasil mengupdate data!', 2000, 'green') 
                        reload_table();
                    }
                    else if (e.status == "gagalkosong") 
                    {
                        Materialize.toast('Tidak ada perubahan pada data!', 2000, 'yellow darken-3') 
                    }                    
                    else{
                        Materialize.toast('Terjadi Kesalahan pada server!', 2000, 'red') 
                        reload_table();                        
                    }
                },
            })   
    }    



    
    function editData(a) {
            $.ajax({
                url: "admin/get_datamobil",
                data: {
                    id: a,
                },
                dataType:'json',
                type: "POST",
                beforeSend: function () {
                    Materialize.toast('Mengambil data..', 2000)  
                },                
                success: function(e) {
                  $("#no_kerangka_edit").val(e.nomor_kerangka);
                  $("#no_polisi_edit").val(e.nomor_polisi);
                  $("#merek_edit").val(e.merek)
                  $("#tipe_edit").val(e.tipe)
                  $("#tahun_edit").val(e.tahun)
                  let element = document.getElementById('tahun_edit');
                  element.value = e.tahun;                  
                  $("#idhidden").val(a)
                  $("#modalEdit").modal('open');

                },
            })         
    }


    function deleteData(a) {
        swal({
            title: "Hapus data ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "HAPUS",
            cancelButtonText: "KEMBALI"
        }).then(function() {
            $.ajax({
                url: "admin/delete_data",
                data: {
                    id: a,
                },
                dataType:'json',
                type: "POST",
                success: function(e) {
                    if (e.status == "sukses") 
                    {
                        Materialize.toast('berhasil menghapus data', 2000, 'green')  
                        reload_table();                        
                    }
                    else{
                         Materialize.toast('Terjadi kesalahan pada server', 2000, 'red')  
                        reload_table();                        

                    }
                },
            })
        })        
    }

    function ceksedia_table() {
        var valuesdata = $('.dataTables_filter input').val();
        if (valuesdata != "" || valuesdata != null) {
            reload_table();
        }
    }
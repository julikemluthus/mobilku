-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 03 Des 2019 pada 17.36
-- Versi Server: 5.7.18-0ubuntu0.16.04.1
-- PHP Version: 7.0.33-0ubuntu0.16.04.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mobilku`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_mobil`
--
CREATE DATABASE mobilku;

USE mobilku;

CREATE TABLE `data_mobil` (
  `id` int(11) NOT NULL,
  `nomor_kerangka` varchar(30) NOT NULL,
  `nomor_polisi` varchar(30) DEFAULT NULL,
  `merek` varchar(70) DEFAULT NULL,
  `tipe` varchar(30) DEFAULT NULL,
  `tahun` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `data_mobil`
--

INSERT INTO `data_mobil` (`id`, `nomor_kerangka`, `nomor_polisi`, `merek`, `tipe`, `tahun`) VALUES
(1, 'GF-2412', 'FR2 1245F', 'Avanza', 'Avanza Veloz', '2003'),
(5, 'GF-IN 141249KS', 'FGW12 SFT2', 'Nissan', 'X-Trail', '2020');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `name`) VALUES
(2, 'admin@dummy.com', 'admin', 'admin'),
(3, 'user', 'user', 'user'),
(6, 'litebig', 'litebig', 'member');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_mobil`
--
ALTER TABLE `data_mobil`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nomor_kerangka` (`nomor_kerangka`),
  ADD KEY `merek` (`merek`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_mobil`
--
ALTER TABLE `data_mobil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
